package kernel.user;


import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

public class Logger {
    private static List<User> users = new ArrayList<User>();

    public static boolean userExists(String name) {
        for (User user : users) {
            if (user.equals(name)) {
                return true;
            }
        }
        return false;
    }

    public static void addUser(User newUser) {
        for (User existingUser : users) {
            if (existingUser.equals(newUser)) {
                throw new IllegalStateException("The user alreay exists");
            }
        }
        users.add(newUser);
    }

    public static User getUser(String name) {
        for (User user : users) {
            if (user.equals(name)) {
                return user;
            }
        }
        throw new InvalidParameterException("Undefined user with name: " + name);
    }


}
