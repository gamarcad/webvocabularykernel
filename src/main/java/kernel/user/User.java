package kernel.user;

import kernel.translate.TranslateRequest;
import kernel.translate.Translation;
import kernel.translate.TranslationsBuilder;
import kernel.translate.TranslationsManager;

public class User {
    private String name;

    private long numberOfTriedTranslations;
    private long numberOfFoundTranslations;

    private TranslationsManager translationsManager;


    public User(String name) {
        this.name = name;
        translationsManager = TranslationsBuilder.buildTranslationsManager(this);
    }

    public void notifyResponse(TranslateRequest.TranslateResponse response) {
        ++numberOfTriedTranslations;
        if (response.isValidTranslation()) {
            ++numberOfFoundTranslations;

            Translation translation = response.getTranslation();

            translationsManager.removeTranslation(translation);
        }
    }

    public long getNumberOfTriesTranslations() {
        return numberOfTriedTranslations;
    }

    public long getNumberOfFoundTranslations() {
        return numberOfFoundTranslations;
    }

    public double getFoundPercentage() {
        if (hasTriedTranslation()) {
            double foundRatio =  (double)(numberOfFoundTranslations) / (double)(numberOfTriedTranslations);
            return foundRatio * 100;
        } else {
            return 0;
        }
    }

    public boolean hasFoundAll() {
        return !translationsManager.containsTranslations();
    }

    public long getNumberOfTriedTranslations() {
        return numberOfTriedTranslations;
    }

    public TranslationsManager getTranslationsManager() {
        return translationsManager;
    }

    public boolean hasTriedTranslation() {
        return numberOfTriedTranslations != 0;
    }

    public boolean hasFoundTranslation() {
        return numberOfFoundTranslations != 0;
    }

    public String getName() {
        return name;
    }

    public boolean equals(String s) {
        return s != null && s.equals(name);
    }

    public String toString() {
        return "{ user: " + name + ", "
                + " stats: { "
                + "tried: " + numberOfTriedTranslations + ", "
                + "found: " + numberOfFoundTranslations + ", "
                + "percentage: " + (hasTriedTranslation() ? getFoundPercentage()  + "%" : "NaN")
                + "}";
    }


}
