package kernel.translate;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Translation {
    private static long globalID;

    public long id;

    private List<String> translations;

    public Translation(String... words) {
        id = globalID;
        ++globalID;
        this.translations = new ArrayList<>();
        addTranslation(words);
    }


    public void addTranslation(String... words) {
        for (String word : words) {
            this.translations.add(word.trim().toLowerCase());
        }
    }

    public String getRandomWord() {
        Random random = new Random();
        int randomIndex = random.nextInt(translations.size());
        return translations.get(randomIndex);
    }

    public List<String> getTranslationsOf(String word) {
        List<String> res = translations.stream().filter(s -> !s.equals(word)).collect(Collectors.toList());
        return res;
    }

    public boolean isValidTranslation(String word) {
        return this.translations.contains(word);
    }

    public long getId() {
        return id;
    }

    public String toString() {
        return translations.toString();
    }
}
