package kernel.translate;

import kernel.user.User;

import java.security.InvalidParameterException;
import java.util.*;
import java.util.stream.Collectors;

public class TranslationsManager {

    private List<Translation> translations;

    private User user;


    public TranslationsManager(User user) {
        this.user = user;
        this.translations = new ArrayList<>();
    }


    public Translation getRandomTranslation() {
        Random random = new Random();
        int randomIndex = random.nextInt(translations.size());
        return translations.get(randomIndex);
    }

    public TranslateRequest requestTranslation() {
        Translation translation = getRandomTranslation();
        return new TranslateRequest(translation);
    }

    public Collection<Translation> getAllTranslations() {
        return translations;
    }

    public void addTranslation(Translation translation) {
        translations.add(translation);
    }

    public void removeTranslation(Translation translation) {
        translations = translations.stream().filter(trans ->
            trans.getId() != translation.getId()
        ).collect(Collectors.toList());
    }

    public Translation getTranslationFromID(long id) {
        for (Translation translation : translations) {
            if (translation.getId() == id) {
                return translation;
            }
        }
        throw new InvalidParameterException("Undefined translation with id " + id);
    }


    public boolean containsTranslations() {
        return !translations.isEmpty();
    }

    public String toString() {
        return translations.toString();
    }
}
